module.exports = {
    collectCoverageFrom: [
        "*/**/*.{js,jsx,ts,tsx}",
        "!**/*.d.ts",
        "!**/node_modules/**",
        "!config/**/*",
        "!coverage/**/*",
    ],
    setupFilesAfterEnv: ["<rootDir>/jest.setup.ts"],
    testPathIgnorePatterns: ["/node_modules/"],
    transform: {
        "^.+\\.(js|jsx|ts|tsx)$": "<rootDir>/node_modules/babel-jest",
    },
    transformIgnorePatterns: ["/node_modules/", "^.+\\.module\\.(css|sass|scss)$"],
    moduleNameMapper: {
        "^.+\\.module\\.(css|scss)$": "<rootDir>/node_modules/jest-css-modules",
        "^components(.*)$": "<rootDir>/src/components$1",
        "^app(.*)$": "<rootDir>/src/app$1",
        "^features(.*)$": "<rootDir>/src/features$1",
        "^config(.*)$": "<rootDir>/src/config$1",
    },
    testMatch: ["**/__tests__/*.(spec|test).(ts|tsx)"],
};
