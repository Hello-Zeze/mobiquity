# Mobiquity Frontend Assessment

## Requirement
Create a web application that uses http://ergast.com/mrd to create a single page application that presents a list that shows the F1 world champions starting from 2005 until
now. Clicking on an item shows the list of the winners for every race for the
selected year. We also request to highlight the row when the winner has
been the world champion in the same season.

## How To Run The Application
- To run locally:
  - In the root dir, execute ```npm install``` to install npm packages
  - Start up the application, execute ```npm start```. The application will start on port ```3000```

## Application Execution:
When the application starts up, it will render a page listing all the F1 Seasons of interest. For this assessment, we are interested in seasons from 2005 until 2021.

Once a season item is clicked upon, the app will navigate to the Season Details page ```seasonDetails.tsx```.

In the Season Details page, there are two sections. 
- The section on the left lists all the races within the selected season. The first race will be selected by default.
- The section on the right, will display the race details along with the race results thereof.

## Hosting
The application is hosted on a personal dev experiments CloudFlare webworker at this address: ```https://web.qhafaza.com```

It is publishable to the same address with this command: ```npm run cf:publish```