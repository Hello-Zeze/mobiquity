export const BASE_URL = "https://ergast.com/api/f1";
export const START_YEAR = 2005;
export const END_YEAR = 2021;
export const F1_BEGINNING_YEAR = 1950;
export const SEASONS_ENDPOINT = "/seasons.json";
export const DRIVERSTANDINGS_ENDPOINT = "/driverStandings.json";
export const COUNTRY_FLAG_ENDPOINT = "https://purecatamphetamine.github.io/country-flag-icons/3x2/";