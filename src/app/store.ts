import { configureStore, ThunkAction, Action } from "@reduxjs/toolkit";
import seasonsReducer from "../features/seasons/seasonsSlice";
import worldChampionReducer from "../features/seasonWorldChampion/seasonWorldChampionSlice";
import seasonRoundsReducer from "../features/seasonRounds/seasonRoundsSlice";
import seasonRoundRacesReducer from "../features/seasonRoundRaces/seasonRoundRacesSlice";

export const store = configureStore({
    reducer: {
        seasons: seasonsReducer,
        seasonWorldChampion: worldChampionReducer,
        seasonRounds: seasonRoundsReducer,
        seasonRoundRaces: seasonRoundRacesReducer
    }
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;