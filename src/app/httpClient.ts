import { HttpResult } from "../types";

export const HttpClient = <T>(resource: string, data?: any): Promise<HttpResult<T>> => {
    return new Promise<HttpResult<T>>(async (resolve, reject) => {
        try {
            const response = await fetch(resource, {
                method: (data) ? "POST" : "GET",
                body: (data)? JSON.stringify(data):null,
                mode: "cors"
            });

            if (response.ok) {
                const data = await response.json();
                resolve({success: true, result: data});
            } else {
                reject(response.statusText);
            }
        } catch (error) {
             reject(error);
        }
    });
};
