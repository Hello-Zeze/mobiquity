import { Status } from "../../types";
export interface SeasonWorldChampionState {    
    seasonWorldChampion: {[key:string]:Driver};
    error?: string;
    status: Status;    
};

export type DriverStandingsTable = {
    MRData: {
        xmlns: string;
        series: string;
        url: string;
        limit: number;
        offset: number;
        total: number;
        StandingsTable: {
            season: string;
            round: number;
            StandingsLists: Standing[];
        };
    };    
};

export type Driver = {
    driverId: string;
    code: string;
    url: string;
    givenName: string;
    familyName: string;
    dateOfBirth: string;
    nationality: string;
};

export type Constructor = {
    constructorId: string;
    url: string;
    name: string;
    nationality: string;
};

export type Standing = {
    season: string;
    round: number;
    DriverStandings: DriverStanding[]
};

export type DriverStanding = {
    position: number;
    positionText: string;
    points: number;
    wins: number;
    Driver: Driver;
    Constructors: Constructor[];
};