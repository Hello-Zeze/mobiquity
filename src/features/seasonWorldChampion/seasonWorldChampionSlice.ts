import { createSlice, createAsyncThunk, PayloadAction } from "@reduxjs/toolkit";
import { HttpResult, Status } from "../../types";
import { SeasonWorldChampionState, DriverStandingsTable } from "./types";
import { HttpClient } from "../../app/httpClient";
import { BASE_URL, DRIVERSTANDINGS_ENDPOINT } from "../../config";
import { RootState } from "../../app/store";

const initialState: SeasonWorldChampionState = {    
    seasonWorldChampion: {},
    status: Status.Idle,    
};

export const fetchSeasonWorldChampion = createAsyncThunk("seasons/fetchSeasonWorldChampion", async ({season}:{season: string}) => {
    return await HttpClient<DriverStandingsTable>(`${BASE_URL}/${season}${DRIVERSTANDINGS_ENDPOINT}`);    
});

const seasonWorldChampionSlice = createSlice({
    name: "seasonWorldChampion",
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(fetchSeasonWorldChampion.pending, (state, _) => {
                state.status = Status.Loading;
            })
            .addCase(fetchSeasonWorldChampion.fulfilled, (state, action: PayloadAction<HttpResult<DriverStandingsTable>>) => {
                if (action.payload.success && action.payload.result) {
                    const { StandingsTable: { season, StandingsLists } } = action.payload.result.MRData;
                    state.seasonWorldChampion[season] = StandingsLists[0].DriverStandings[0].Driver;
                    state.status = Status.Idle;
                } else {
                    state.error = action.payload.error;
                    state.status = Status.Failed;
                }
            })
            .addCase(fetchSeasonWorldChampion.rejected, (state, action) => {
                state.status = Status.Failed;
                state.error = action.error.message??"An error ocurred while loading season world champion.";
            })
    }
});

export const selectSeasonWorldChampion = ({seasonWorldChampion: { seasonWorldChampion }}: RootState, season: string) => seasonWorldChampion[`${season}`];

export default seasonWorldChampionSlice.reducer;