import { createSlice, createAsyncThunk, PayloadAction } from "@reduxjs/toolkit";
import { HttpResult, Status } from "../../types";
import { SeasonRoundsState, RaceTable, Race } from "./types";
import { HttpClient } from "../../app/httpClient";
import { BASE_URL } from "../../config";
import { RootState } from "../../app/store";

const initialState: SeasonRoundsState = {    
    seasonRounds: {},    
    status: Status.Idle,    
};

export const fetchSeasonRounds = createAsyncThunk("seasons/fetchSeasonRounds", async ({season}:{season: string}) => {
    return await HttpClient<RaceTable>(`${BASE_URL}/${season}.json`);    
});

const seasonRoundsSlice = createSlice({
    name: "seasonRounds",
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(fetchSeasonRounds.pending, (state, _) => {
                state.status = Status.Loading;
            })
            .addCase(fetchSeasonRounds.fulfilled, (state, action: PayloadAction<HttpResult<RaceTable>>) => {
                if (action.payload.success && action.payload.result) {
                    const { RaceTable: { season, Races } } = action.payload.result.MRData;
                    state.seasonRounds[season] = Races.sort((a: Race,b: Race) => { return (a.round-b.round); });
                    state.status = Status.Idle;
                } else {
                    state.error = action.payload.error;
                    state.status = Status.Failed;
                }
            })
            .addCase(fetchSeasonRounds.rejected, (state, action) => {
                state.status = Status.Failed;
                state.error = action.error.message??"An error ocurred while loading season rounds.";
            });
    }
});


export const selectSeasonRounds = ({seasonRounds: { seasonRounds }}: RootState, season: string) => seasonRounds[season];

export default seasonRoundsSlice.reducer;