import { Status } from "../../types";

export interface SeasonRoundsState {    
    seasonRounds: {[key:string]: Race[]};
    error?: string;
    status: Status;    
};

export type RaceTable = {
    MRData: {
        xmlns: string;
        series: string;
        url: string;
        limit: number;
        offset: number;
        total: number;
        RaceTable: {
            season: string;
            round: number;
            Races: Race[];
        };
    };    
};

export type Race = {
    season: string;
    round: number;
    url: string;
    raceName: string;
    Circuit: Circuit;
    date: string;
    time: string;
};

export type Circuit = {
    circuitId: string;
    url: string;
    circuitName: string;
    Location: {
        lat: number;
        long: number
        locality: string;
        country: string;
    }
};