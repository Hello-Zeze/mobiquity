import { Status } from "../../types";
export interface SeasonRoundRacesState {    
    seasonRoundRaces: {[key:string]: RaceData};    
    error?: string;
    status: Status;
};

export type RaceResultTable = {
    MRData: {
        xmlns: string;
        series: string;
        url: string;
        limit: number;
        offset: number;
        total: number;
        RaceTable: {
            season: string;
            round: number;
            Races: RaceResults[];
        };
    };    
};

export type RaceResults = {
    season: string;
    round: number;
    url: string;
    raceName: string;
    Circuit: Circuit;
    date: string;
    time: string;
    Results: RaceResult[];
};

export type Circuit = {
    circuitId: string;
    url: string;
    circuitName: string;
    Location: {
        lat: number;
        long: number
        locality: string;
        country: string;
    }
};

export type RaceData = {
    raceDate: string;
    raceCircuit: string;
    raceResults: RaceResult[];
};

export type RaceResult = {
    number: number,
    position: number;
    positionText: string;
    points: number;
    Driver: Driver;
    Constructor: Constructor;
    grid: number;
    laps: number;
    status: string;
    Time: {
        millis: number;
        time: string;
    };
    FastestLap: Lap;
};

export type Driver = {
    driverId: string;
    code: string;
    url: string;
    givenName: string;
    familyName: string;
    dateOfBirth: string;
    nationality: string;
};

export type Constructor = {
    constructorId: string;
    url: string;
    name: string;
    nationality: string;
};

export type Lap = {
    rank: number;
    lap: number;
    Time: {
        time: string;
    };
    AverageSpeed: {
        units: string;
        speed: string;
    };
};