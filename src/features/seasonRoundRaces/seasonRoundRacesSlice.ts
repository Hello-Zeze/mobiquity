import { createSlice, createAsyncThunk, PayloadAction } from "@reduxjs/toolkit";
import { HttpResult, Status } from "../../types";
import { SeasonRoundRacesState, RaceResultTable, RaceData, RaceResult } from "./types";
import { HttpClient } from "../../app/httpClient";
import { BASE_URL } from "../../config";
import { RootState } from "../../app/store";

const initialState: SeasonRoundRacesState = {    
    seasonRoundRaces: {},    
    status: Status.Idle,
};

export const fetchSeasonRoundResults = createAsyncThunk("seasons/fetchSeasonRoundResults", async ({season, round}:{season: string, round: number}) => {
    return await HttpClient<RaceResultTable>(`${BASE_URL}/${season}/${round}/results.json`);    
});

const seasonRoundRacesSlice = createSlice({
    name: "seasonRoundRaces",
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(fetchSeasonRoundResults.pending, (state, _) => {
                state.status = Status.Loading;
            })
            .addCase(fetchSeasonRoundResults.fulfilled, (state, action: PayloadAction<HttpResult<RaceResultTable>>) => {
                if (action.payload.success && action.payload.result) {
                    const { RaceTable: { season, round, Races } } = action.payload.result.MRData;
                    const raceData: RaceData = {
                        raceCircuit: Races[0].Circuit.circuitName,
                        raceDate: Races[0].date,
                        raceResults: Races[0].Results.sort((a: RaceResult, b: RaceResult) => (a.position - b.position)),
                    };
                    state.seasonRoundRaces[`${season}:${round}`] = raceData;
                    state.status = Status.Idle;
                } else {
                    state.error = action.payload.error;
                    state.status = Status.Failed;
                }
            })
            .addCase(fetchSeasonRoundResults.rejected, (state, action) => {
                state.status = Status.Failed;
                state.error = action.error.message??"An error ocurred while loading season round race results.";
            });
    }
});

export const selectSeasonRoundResults = ({seasonRoundRaces: { seasonRoundRaces }}: RootState, season: string, round: number) => seasonRoundRaces[`${season}:${round}`];

export default seasonRoundRacesSlice.reducer;