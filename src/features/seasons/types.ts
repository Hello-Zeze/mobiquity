import { Status } from "../../types";
export interface SeasonState {
    seasons: Season[];    
    error?: string;
    status: Status;    
};

export type Season = {
    season: string;
};

export type SeasonsTable = {
    MRData: {
        xmlns: string;
        series: string;
        url: string;
        limit: number;
        offset: number;
        total: number;
        SeasonTable: {
            Seasons: Season[];
        };
    };
};