import { createSlice, createAsyncThunk, PayloadAction } from "@reduxjs/toolkit";
import { HttpResult, Status } from "../../types";
import { SeasonState, SeasonsTable } from "./types";
import { HttpClient } from "../../app/httpClient";
import { BASE_URL, SEASONS_ENDPOINT, F1_BEGINNING_YEAR, START_YEAR, END_YEAR } from "../../config";
import { RootState } from "../../app/store";

const initialState: SeasonState = {
    seasons: [],    
    status: Status.Idle,    
};

export const fetchSeasons = createAsyncThunk("seasons/fetchSeasons", async () => {
    const offset = (START_YEAR - F1_BEGINNING_YEAR); //Here we are calculating the offset for the results we want back from the api based on when F1 began and the year we want to start at
    const limit = ((END_YEAR + 1) - START_YEAR); //Here we are calculating the range of years we are interested in. This is inclusive of the end year.
    return await HttpClient<SeasonsTable>(`${BASE_URL}${SEASONS_ENDPOINT}?offset=${offset}&limit=${limit}`);    
});

const seasonsSlice = createSlice({
    name: "seasons",
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(fetchSeasons.pending, (state, _) => {
                state.status = Status.Loading;
            })
            .addCase(fetchSeasons.fulfilled, (state, action: PayloadAction<HttpResult<SeasonsTable>>) => {
                if (action.payload.success && action.payload.result) {
                    const { SeasonTable: { Seasons } } = action.payload.result.MRData;
                    state.seasons = Seasons;
                    state.status = Status.Succeeded;
                } else {
                    state.error = "Something went wrong.";
                    state.status = Status.Failed;
                }
            })
            .addCase(fetchSeasons.rejected, (state, action) => {
                state.status = Status.Failed;
                state.error = action.error.message??"An error ocurred while loading seasons.";
            });
    }
});

export const selectAllSeasons = ({seasons: { seasons }}: RootState) => seasons;

export default seasonsSlice.reducer;