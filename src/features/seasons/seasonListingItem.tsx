import React from "react";
import "./season.css";
interface SeasonListingItemProps {
    season: string;    
};

const SeasonListingItem: React.FC<SeasonListingItemProps> = ({ season }) => {
    return (
        <div>
            <h1 className="season-item-header">{`F1 Season: ${season}`}</h1>
        </div>
    )
}

export default SeasonListingItem;
