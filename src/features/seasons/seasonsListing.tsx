import React, { useEffect } from "react";
import SeasonListingItem from "./seasonListingItem";
import { Link } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { fetchSeasons, selectAllSeasons } from "./seasonsSlice";
import { Status } from "../../types";
import SimpleBar from "simplebar-react";
import LoadingIndicator from "../../components/loading";
import "./season.css";

const SeasonsListing: React.FC = () => {
    const dispatch = useAppDispatch();
    const seasons = useAppSelector(selectAllSeasons);
    const seasonsStatus = useAppSelector(state => state.seasons.status);    
    
    useEffect(() => {
        if (seasonsStatus === Status.Idle) {
            dispatch(fetchSeasons());
        }
    }, [dispatch, seasonsStatus]);   
    
    return (
        <>
            {seasons ? (
                <SimpleBar className="season-listing">
                    {seasons.map(item => {
                        return (
                            <div className="season-item">
                                <Link key={item.season} to={`/${item.season}`}>
                                    <SeasonListingItem
                                        key={item.season}
                                        season={item.season}
                                    />
                                </Link>
                            </div>
                        );
                    })}
                </SimpleBar>
            ):(
                <LoadingIndicator />
            )}
        </>
    )
}

export default SeasonsListing;
