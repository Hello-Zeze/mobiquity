import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { fetchSeasonRounds, selectSeasonRounds } from "../seasonRounds/seasonRoundsSlice";
import { selectSeasonRoundResults, fetchSeasonRoundResults } from "../seasonRoundRaces/seasonRoundRacesSlice";
import { fetchSeasonWorldChampion, selectSeasonWorldChampion } from "../seasonWorldChampion/seasonWorldChampionSlice";
import { Status } from "../../types";
import DriverPosition from "../../components/driverPosition";
import SimpleBar from "simplebar-react";
import LoadingIndicator from "../../components/loading";
import "./season.css";

const SeasonDetails: React.FC = () => {
    const dispatch = useAppDispatch();
    const { season } = useParams<{season: string}>();
    const [selectedRound, setSelectedRound] = useState<number>(1);
    const seasonRoundsStatus = useAppSelector(state => state.seasonRounds.status);
    const seasonRoundRacesStatus = useAppSelector(state => state.seasonRoundRaces.status);
    
    const seasonRounds = useAppSelector(state => selectSeasonRounds(state, season));
    const seasonRoundResults = useAppSelector(state => selectSeasonRoundResults(state, season, selectedRound));    
    const seasonWorldChampion = useAppSelector(state => selectSeasonWorldChampion(state, season));

    useEffect(() => {
        if (seasonRoundsStatus === Status.Idle && !seasonWorldChampion) {
            dispatch(fetchSeasonRounds({ season }));
            dispatch(fetchSeasonWorldChampion({ season }));
        }
    }, [seasonRoundsStatus, dispatch, season, seasonWorldChampion]);

    useEffect(() => {
        if (seasonRoundRacesStatus === Status.Idle && !seasonRoundResults) {
            dispatch(fetchSeasonRoundResults({season, round: selectedRound}));
        }
    }, [seasonRoundRacesStatus, dispatch, selectedRound, seasonRoundResults, season]);

    const onRaceSelected = (roundNumber: number) => {
        const seasonRound = seasonRounds.find(seasonRound => seasonRound.round === roundNumber);
        if (seasonRound) {
            setSelectedRound(seasonRound.round);
        }
    };

    const isSeasonWorldChampion = (driverId: string) => {
        return (seasonWorldChampion && seasonWorldChampion.driverId === driverId);
    }

    const getRaceItemStyling = (seasonRound: number, currentRound: number) => {
        if (seasonRound == currentRound) {
            return "season-details-race-item selected-season-details-race-item";
        }
        return "season-details-race-item";
    }

    return (
        <div className="season-details-container">
            <div className="season-details-race-items">
                {seasonRounds? (
                    seasonRounds.map(seasonRound => {
                        return (
                            <div 
                                key={seasonRound.round} 
                                onClick={()=>{onRaceSelected(seasonRound.round)}}
                                className={getRaceItemStyling(seasonRound.round, selectedRound)}
                            >
                                {seasonRound.raceName}
                            </div>
                        );
                    })
                ):null}
            </div>
            <div className="season-race-results-container">
                {seasonRoundResults ? (
                    <>
                        <h2>Race Details</h2>
                        <div className="race-data">{`Date: ${seasonRoundResults.raceDate}`}</div>
                        <div className="race-data">{`Circuit: ${seasonRoundResults.raceCircuit}`}</div>
                        <h3>Results</h3>
                        <SimpleBar className="race-results">
                            {seasonRoundResults.raceResults.map(resultItem => {
                                return (
                                    <DriverPosition
                                        key={resultItem.position}
                                        constructorName={resultItem.Constructor.name}
                                        familyName={resultItem.Driver.familyName}
                                        givenName={resultItem.Driver.givenName}
                                        nationality={resultItem.Driver.nationality}
                                        position={resultItem.position}
                                        isWorldChampion={isSeasonWorldChampion(resultItem.Driver.driverId)}
                                    />
                                );
                            })}
                        </SimpleBar>
                    </>
                ):(
                    <LoadingIndicator />
                )}
            </div>
        </div>
    )
}

export default SeasonDetails;
