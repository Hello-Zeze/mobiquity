import React from "react";
import "./season.css";

interface SeasonRaceRoundDetailsProps {
    country: string;
    locality: string;
    raceName: string;
}

const SeasonRaceRoundDetails: React.FC<SeasonRaceRoundDetailsProps> = ({ raceName, country, locality }) => {
    return (
        <div className="season-item-race">
            <div className="sm-nationality-flag"></div>
            <div className="season-item-race-rounds">
                <div className="season-item-racename">{raceName}</div>
                <div className="season-item-race-locality">{`${locality}, ${country}`}</div>
            </div>
        </div>
    );
}

export default SeasonRaceRoundDetails
