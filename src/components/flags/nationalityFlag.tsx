import React from "react";
import { nationalities } from "./countries";
import { COUNTRY_FLAG_ENDPOINT } from "../../config";
import "./nationalityFlag.css";

export interface NationalityFlagProps {
    countryName: string;
};

const NationalityFlag: React.FC<NationalityFlagProps> = ({ countryName }) => {
    const nationalityFlag = `${COUNTRY_FLAG_ENDPOINT}${nationalities[countryName]}.svg`;
    return (
        <div className="nationality-flag">
            <img src={nationalityFlag} alt={"Ergast Developer Api"} />
        </div>
    )
}

export default NationalityFlag;
