import React from "react";
import { 
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import { AppRoute } from "./types";
import SeasonListing from "../../features/seasons/seasonsListing";
import SeasonDetails from "../../features/seasons/seasonDetails";
import Nagivation from "../navigation";
import "./layout.css";

const routing: Array<AppRoute> = [
    {
        path: "/",
        exact: true,
        component: () => <SeasonListing />
    },
    {
        path: "/:season",
        exact: true,
        component: () => <SeasonDetails />
    },
];

const Layout: React.FC = () => {
    return (
        <Router>
            <Nagivation />
            <div className="layout-container">
                <Switch>
                    {routing.map((route, index) => {
                        return (
                            <Route
                                key={index}
                                path={route.path}
                                exact={route.exact}
                                children={<route.component />}
                            />
                        );
                    })}
                </Switch>
            </div>
        </Router>
    );
}

export default Layout;
