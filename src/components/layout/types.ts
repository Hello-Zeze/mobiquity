
export type AppRoute = {
    path: string;
    exact: boolean;
    component: () => JSX.Element;    
}