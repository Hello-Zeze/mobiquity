import React from "react";
import "./loading.css";

const LoadingIndicator: React.FC = () => {
    return (
        <div className="loading-indicator">
            <div className="loadingio-spinner-ellipsis-wduzi6nacf">
                <div className="ldio-ewo14rykz36">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>
    )
}

export default LoadingIndicator;
