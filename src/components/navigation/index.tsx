import React from "react";
import { Link, useLocation } from "react-router-dom";
import "./navigation.css";

const Navigation: React.FC = () => {
    const location = useLocation();
    const isHomePage = () => {
        return (location.pathname === "/");
    }
    return (
        <div className="navigation-container">
            <div className="navigation-content">
                <div className="nav-link">
                    {!isHomePage()? (
                        <Link to="/">
                            Back to F1 Seasons
                        </Link>
                    ):null}
                </div>
                <div className="app-title">
                    Mobiquity Ergast
                </div>
            </div>
        </div>
    )
}

export default Navigation;
