import React from "react";
import NationalityFlag from "../flags/nationalityFlag";
import "./driverPosition.css";

export interface DriverPositionProps {
    position: number;
    nationality: string;
    givenName: string;
    familyName: string;
    constructorName: string;
    isWorldChampion: boolean;
};

const DriverPosition: React.FC<DriverPositionProps> = ({
    position,
    nationality,
    givenName,
    familyName,
    constructorName,
    isWorldChampion
}) => {
    return (
        <div className="driver-position-item">
            <div className="driver-position">{position}</div>
            <NationalityFlag countryName={nationality} />
            <div className="race-driver-details">
                <div className="race-driver-name">{`${givenName} ${familyName}`}</div>
                <div className="race-driver-constructor">{constructorName}</div>
            </div>
            {isWorldChampion? (
                <div className="world-champ">world champion</div>
            ):null}
        </div>
    )
}

export default DriverPosition;
