export enum Status {
    Idle = "Idle",
    Loading = "Loading",
    Succeeded = "Succeeded",
    Failed = "Failed"
};

export type HttpResult<T> = {
    success: boolean;
    error?: string;
    result?: T;
};