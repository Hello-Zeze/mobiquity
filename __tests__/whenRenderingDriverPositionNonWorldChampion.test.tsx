import React from "react";
import DriverPosition from "../src/components/driverPosition";
import { render } from "@testing-library/react";

describe("DriverPosition Component", () => {
    describe("When rendering DriverPosition for non-WorldChampion driver", () => {
        it("should render without 'WORLD CHAMPION' badge", () => {
            const constructorName = "RedBull";
            const driverFamilyName = "Hamilton";
            const driverGivenName = "Lewis";
            const driverNationality = "British";
            const position = 1;

            const { container } = render(
                <DriverPosition
                    constructorName={constructorName}
                    familyName={driverFamilyName}
                    givenName={driverGivenName}
                    nationality={driverNationality}
                    position={position}
                    isWorldChampion={false}
                />,
            );

            const worldChampionBadge = container.querySelector(".world-champ");

            expect(worldChampionBadge).toBeUndefined();
        });
    });
});